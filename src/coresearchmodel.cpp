/*
    *
    * This file is a part of CoreHunt.
    * A file search utility for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include "coresearchmodel.h"


QStringList CoreSearchModel::categories = QStringList();

CoreSearchModel::CoreSearchModel() : QAbstractItemModel()
{
	categories << "All" << "Folders" << "Images" << "Media" << "Documents" << "Text" << "Others";

	Q_FOREACH (const QString &category, categories) {
        categoryItemMap.insert(category, CoreSearchItemList());
    }
}

QModelIndex CoreSearchModel::index(int row, int column, const QModelIndex &parent) const
{
    if (row < 0 || column < 0) {
		return QModelIndex();
    }

    if ((row >= rowCount(parent)) or (column >= columnCount(parent))) {
		return QModelIndex();
    }

    CoreSearchItem *item = categoryItemMap[ mCategory ].value(row);

    if (item) {
        return createIndex(row, column, item);
	}

	return QModelIndex();
}

QModelIndex CoreSearchModel::parent(const QModelIndex &) const
{
    return QModelIndex();
}

int CoreSearchModel::rowCount(const QModelIndex &) const
{
    return categoryItemMap[ mCategory ].size();
}

int CoreSearchModel::columnCount(const QModelIndex &) const
{
	/* Fixed number of columns: Path, Type */
    return 2;
}

QVariant CoreSearchModel::data(const QModelIndex &index, int role) const
{
    CoreSearchItem *item = static_cast<CoreSearchItem *>(index.internalPointer());

	if (role == Qt::DisplayRole) {
		if (index.column() == 0)
			return item->path();
		else if (index.column() == 1)
			return item->type();

		return QString();
	} else if (role == Qt::DecorationRole) {
		if (index.column() != 0)
			return QIcon();

		return item->icon();
	}

//    switch (role) {
//		case Qt::DisplayRole: {
//            if (index.column() == 0) {
//				return item->path();
//            } else if (index.column() == 1) {
//				return item->type();
//            } else {
//				return QString();
//            }
//        }

//		case Qt::DecorationRole: {
//            if (index.column() != 0) {
//				return QIcon();
//            }

//			return item->icon();
//		}
//	}

	return QVariant();
}

QVariant CoreSearchModel::headerData(int section, Qt::Orientation orientation, int role) const
{

    if (orientation != Qt::Horizontal) {
		return QVariant();
    }

    switch (role) {
		case Qt::DisplayRole: {
            if (section == 0) {
				return "File name";
            } else if (section == 1) {
				return "File type";
            } else {
				return QString();
            }
        }

		default: {
			return QVariant();
		}
	}
}

bool CoreSearchModel::setCategory(const QString &ctg)
{
    if (categories.contains(ctg)) {
        beginResetModel();
		mCategory = ctg;
		endResetModel();

		return true;
	}

	return false;
}

bool CoreSearchModel::addCategory(const QString &ctg) const
{
	/* We create @ctg only if it does not exists */
    if (not categories.contains(ctg)) {
        categoryItemMap.insert(ctg, CoreSearchItemList());
		return true;
	}

	return false;
}

void CoreSearchModel::addResult(const QString &category, CoreSearchItem *item)
{
    if (categories.contains(category)) {
		categoryItemMap[ category ] << item;
    }

	/* A dirty HACK: I don't know how to do this properly. */
    if (mCategory == category) {
        beginInsertRows(QModelIndex(), 0, 0);
		endInsertRows();
	}
}

void CoreSearchModel::addResults(const QString &category, CoreSearchItemList &items)
{
	int count = rowCount();

    if (mCategory == category) {
        beginInsertRows(QModelIndex(), count, count + items.count() - 1);
    }

    if (categories.contains(category)) {
		categoryItemMap[ category ] << items;
    }

	/* Update if the current category was edited */
    if (mCategory == category) {
		endInsertRows();
	}
}

void CoreSearchModel::clear()
{
	beginResetModel();

	categories.clear();
	categoryItemMap.clear();

	categories << "All" << "Folders" << "Images" << "Media" << "Documents" << "Text" << "Others";

    Q_FOREACH (QString category, categories) {
        categoryItemMap.insert(category, CoreSearchItemList());
    }

	endResetModel();
}
